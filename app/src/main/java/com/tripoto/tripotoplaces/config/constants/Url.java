package com.tripoto.tripotoplaces.config.constants;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class Url {

    public static final String BASE_URL = "http://www.tripoto.com/";

    public static final String NEARBY_PLACES = "/ApiPlacesNearby/getPlacesNearby";
}
