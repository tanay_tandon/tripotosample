package com.tripoto.tripotoplaces.config.constants;

/**
 * Created by tanaytandon on 25/01/16.
 */
public class Value {

    public static final double LAT_NOT_SET = 91.0;
    public static final double LNG_NOT_SET = 181.0;

    public static final long ONE_DAY = 24 * 60 * 60 * 1000;
    public static final long FIVE_MIN = 5 * 60 * 1000;

    public static final int WIFI_LIMIT = 30;
    public static final int DATA_LIMIT = 10;
}
