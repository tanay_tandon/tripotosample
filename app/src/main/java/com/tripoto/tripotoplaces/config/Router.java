package com.tripoto.tripotoplaces.config;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

/**
 * Created by tanaytandon on 22/01/16.
 */
public class Router {

    public static void showSettingsActivity(Context context) {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        context.startActivity(intent);
    }
}
