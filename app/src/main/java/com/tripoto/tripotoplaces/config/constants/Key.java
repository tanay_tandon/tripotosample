package com.tripoto.tripotoplaces.config.constants;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class Key {

    // request keys
    public static final String TRIPOTO = "Tripoto";
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String UNIT = "unit";
    public static final String RADIUS = "radius";

    //response keys for user
    public static final String USER_ID = "fk_userid";
    public static final String USER_HANDLE = "user_handle";
    public static final String USER_NAME = "user_name";
    public static final String USER_IMAGE_URL = "user_image_url";
    public static final String USER_IMAGE_FULL_URL = "user_image_full_url";
    public static final String USER_ABOUT_ME = "user_about_me";

    //response keys for trips
    public static final String TRIP_ID = "id";
    public static final String TRIP_NAME = "trip_name";
    public static final String TRIP_FULL_IMAGE_URL = "full_url";
    public static final String TRIP_IMAGE_URL = "trip_image_url";


    public static final String APP_PREFS = "tripoto_places_prefs";
    public static final String LAST_LOCATION = "last_location";
    public static final String USE_CURRENT_LOCATION = "use_current_location";
}
