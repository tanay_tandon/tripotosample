package com.tripoto.tripotoplaces.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.tripoto.tripotoplaces.config.constants.Key;

/**
 * Created by tanaytandon on 25/01/16.
 */
public class PrefUtils {

    private static SharedPreferences mSharedPreferences;
    private static SharedPreferences.Editor mEditor;

    public static void init(Context context) {
        mSharedPreferences = context.getSharedPreferences(Key.APP_PREFS, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mEditor.commit();
    }

    public static void setRadius(int value) {
        mEditor.putInt(Key.RADIUS, value).commit();
    }

    public static int getRadius() {
        return mSharedPreferences.getInt(Key.RADIUS, 100);
    }

    public static void setLastLocation(String value) {
        mEditor.putString(Key.LAST_LOCATION, value).commit();
    }

    public static String getLastLocation() {
       return mSharedPreferences.getString(Key.LAST_LOCATION, null);
    }

    public static void setUsingCurrentLocation(boolean value) {
        mEditor.putBoolean(Key.USE_CURRENT_LOCATION, value).commit();
    }

    public static boolean isUsingCurrentLocation() {
        return mSharedPreferences.getBoolean(Key.USE_CURRENT_LOCATION, true);
    }
}
