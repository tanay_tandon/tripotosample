package com.tripoto.tripotoplaces.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class TripsResponse {

    @SerializedName("result")
    @Expose
    public TripsResult result;
}
