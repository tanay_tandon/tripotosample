package com.tripoto.tripotoplaces.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tripoto.tripotoplaces.config.constants.Key;

public class TripsRequestModel {

    @Expose
    @SerializedName(Key.TRIPOTO)
    public Tripoto tripoto;

    public TripsRequestModel(Tripoto tripoto) {
        this.tripoto = tripoto;
    }
}
