package com.tripoto.tripotoplaces.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tripoto.tripotoplaces.config.constants.Key;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class TripsData {
    @SerializedName(Key.TRIP_ID)
    @Expose
    public String id;
    @SerializedName(Key.USER_HANDLE)
    @Expose
    public String userHandle;
    @SerializedName(Key.TRIP_NAME)
    @Expose
    public String tripName;
    @SerializedName(Key.USER_ID)
    @Expose
    public String fkUserid;
    @SerializedName(Key.USER_NAME)
    @Expose
    public String userName;
    @SerializedName(Key.TRIP_IMAGE_URL)
    @Expose
    public String tripImageUrl;
    @SerializedName(Key.TRIP_FULL_IMAGE_URL)
    @Expose
    public String fullUrl;
    @SerializedName(Key.USER_IMAGE_URL)
    @Expose
    public String userImageUrl;
    @SerializedName(Key.USER_IMAGE_FULL_URL)
    @Expose
    public String userImageFullUrl;
    @SerializedName(Key.USER_ABOUT_ME)
    @Expose
    public String userAboutMe;
}
