package com.tripoto.tripotoplaces.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class TripsResult {

    @SerializedName("data")
    @Expose
    public ArrayList<TripsData> data = new ArrayList<>();
}
