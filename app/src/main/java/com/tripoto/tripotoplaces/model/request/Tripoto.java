package com.tripoto.tripotoplaces.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tripoto.tripotoplaces.config.constants.Key;

public class Tripoto {
    @SerializedName(Key.OFFSET)
    @Expose
    public int offset;
    @SerializedName(Key.LIMIT)
    @Expose
    public int limit;
    @SerializedName(Key.LATITUDE)
    @Expose
    public double latitude;
    @SerializedName(Key.LONGITUDE)
    @Expose
    public double longitude;
    @SerializedName(Key.RADIUS)
    @Expose
    public int radius;
    @SerializedName(Key.UNIT)
    @Expose
    public String unit;

    public Tripoto(int offset, int limit, double latitude, double longitude, int radius, String unit) {
        this.offset = offset;
        this.limit = limit;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.unit = unit;
    }
}
