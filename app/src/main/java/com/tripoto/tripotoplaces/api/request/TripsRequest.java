package com.tripoto.tripotoplaces.api.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.tripoto.tripotoplaces.api.APIInterface;
import com.tripoto.tripotoplaces.model.request.Tripoto;
import com.tripoto.tripotoplaces.model.request.TripsRequestModel;
import com.tripoto.tripotoplaces.model.response.TripsResponse;

/**
 * Created by tanaytandon on 25/01/16.
 */
public class TripsRequest extends RetrofitSpiceRequest<TripsResponse, APIInterface> {

    private Tripoto tripoto;

    public TripsRequest(Tripoto tripoto) {
        super(TripsResponse.class, APIInterface.class);
        this.tripoto = tripoto;
    }

    @Override
    public TripsResponse loadDataFromNetwork() throws Exception {
        return getService().getNearbyPlaces(new TripsRequestModel(this.tripoto));
    }
}
