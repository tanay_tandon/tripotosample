package com.tripoto.tripotoplaces.api;

import com.tripoto.tripotoplaces.config.constants.Url;
import com.tripoto.tripotoplaces.model.request.TripsRequestModel;
import com.tripoto.tripotoplaces.model.response.TripsResponse;

import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by tanaytandon on 24/01/16.
 */
public interface APIInterface {
    @Headers({
            "Content-Type: application/json"
    })
    @POST(Url.NEARBY_PLACES)
    public TripsResponse getNearbyPlaces(@Body TripsRequestModel tripoto);
}
