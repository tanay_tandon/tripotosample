package com.tripoto.tripotoplaces.api.service;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.tripoto.tripotoplaces.config.constants.Url;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class APIService extends RetrofitGsonSpiceService {

    @Override
    protected String getServerUrl() {
        return Url.BASE_URL;
    }
}
