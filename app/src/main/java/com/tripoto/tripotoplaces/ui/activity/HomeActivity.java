package com.tripoto.tripotoplaces.ui.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.exception.CacheSavingException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.tripoto.tripotoplaces.R;
import com.tripoto.tripotoplaces.api.request.TripsRequest;
import com.tripoto.tripotoplaces.api.service.APIService;
import com.tripoto.tripotoplaces.config.Router;
import com.tripoto.tripotoplaces.config.constants.Code;
import com.tripoto.tripotoplaces.config.constants.Key;
import com.tripoto.tripotoplaces.config.constants.Value;
import com.tripoto.tripotoplaces.model.request.Tripoto;
import com.tripoto.tripotoplaces.model.response.TripsData;
import com.tripoto.tripotoplaces.model.response.TripsResponse;
import com.tripoto.tripotoplaces.ui.adapter.TripsAdapter;
import com.tripoto.tripotoplaces.ui.fragment.ParametersFragment;
import com.tripoto.tripotoplaces.util.DeviceUtils;
import com.tripoto.tripotoplaces.util.PrefUtils;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        RequestListener<TripsResponse>, LocationListener {

    public static final String TAG = HomeActivity.class.getName();

    private GoogleApiClient mGoogleApiClient;
    private int radius;
    private SpiceManager mSpiceManager = new SpiceManager(APIService.class);
    private View rootView, pbView;
    private ArrayList<TripsData> dataset;
    private String cacheKey;
    private boolean requestInProgress;
    private TripsAdapter mTripsAdapter;
    private double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.rootView = findViewById(R.id.cl_root);
        this.pbView = findViewById(R.id.pb_trips);

        this.dataset = new ArrayList<>();
        this.mTripsAdapter = new TripsAdapter(this, dataset);
        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this).addConnectionCallbacks(this)
                .addApi(LocationServices.API).build();

        this.radius = PrefUtils.getRadius();
        this.requestInProgress = false;


        setUpRecyclerView();
        doActionOnPermission();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showParametersFragment();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        this.mSpiceManager.start(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(this.brParameters, new IntentFilter(getResources().getString(R.string.intent_filter_set_parameters)));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (this.mSpiceManager.isStarted()) {
            this.mSpiceManager.shouldStop();
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.brParameters);
        if (this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.disconnect();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Code.REQUEST_LOCATION_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.mGoogleApiClient.connect();
                } else {
                    Snackbar.make(rootView, "Location Permission denied, can't proceed", Snackbar.LENGTH_LONG).setAction("Set Location", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showParametersFragment();
                        }
                    }).show();
                }
                break;
            }
        }
    }

    private boolean hasPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }

    @Override
    public void onConnected(Bundle bundle) {
        setLocationAndMakeRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(TripsResponse tripsResponse) {
        Log.d(TAG, "request success");
        try {
            this.requestInProgress = false;
            if (tripsResponse != null && tripsResponse.result != null) {
                mSpiceManager.putDataInCache(cacheKey, tripsResponse);
                this.dataset.addAll(tripsResponse.result.data);
                this.mTripsAdapter.notifyDataSetChanged();
                this.pbView.setVisibility(View.GONE);
            }
        } catch (CacheSavingException e) {
            e.printStackTrace();
        } catch (CacheCreationException e) {
            e.printStackTrace();
        }
    }


    private void setUpRecyclerView() {
        RecyclerView rvTrips = (RecyclerView) findViewById(R.id.rv_trips);
        final LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        rvTrips.setLayoutManager(mLinearLayoutManager);
        rvTrips.setHasFixedSize(true);
        rvTrips.setAdapter(this.mTripsAdapter);
        rvTrips.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = mLinearLayoutManager.getItemCount();
                int lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
                int threshold = DeviceUtils.isWifiConnected(HomeActivity.this) ? 15 : 5;

                if (totalItemCount - threshold <= lastVisibleItem && !requestInProgress) {
                    makeRequest();
                }
            }
        });
    }

    private void doActionOnPermission() {
        if (Build.VERSION.SDK_INT >= 23 && !hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            requestPermission(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, Code.REQUEST_LOCATION_PERMISSION);
        } else {
            if (this.mGoogleApiClient.isConnected()) {
                setLocationAndMakeRequest();
            } else {
                this.mGoogleApiClient.connect();
            }

        }

    }

    private void showParametersFragment() {
        ParametersFragment parametersFragment = ParametersFragment.newInstance();
        parametersFragment.show(getSupportFragmentManager(), ParametersFragment.TAG);
    }

    private void setLocationAndMakeRequest() {
        if (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            try {
                Log.d(TAG, "setting location");
                Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if(location != null) {
                    this.latitude = location.getLatitude();
                    this.longitude = location.getLongitude();
                    Log.d(TAG, "lat " + latitude + " lng " + longitude);
                    makeRequest();
                } else {
                    Snackbar sbParams = Snackbar.make(rootView, "Location not available", Snackbar.LENGTH_LONG);
                    sbParams.setAction("Set Location", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showParametersFragment();
                        }
                    });
                    sbParams.show();
//                    LocationRequest locationRequest = new LocationRequest();
//                    locationRequest.setInterval(Value.FIVE_MIN);
//                    locationRequest.setFastestInterval(Value.FIVE_MIN);
//                    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
//                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
                }
            } catch (SecurityException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void makeRequest() {
        if(DeviceUtils.isNetworkConnected(this)) {
            int limit = DeviceUtils.isWifiConnected(HomeActivity.this) ? Value.WIFI_LIMIT : Value.DATA_LIMIT;
            Tripoto tripoto = new Tripoto(dataset.size(), limit, this.latitude, this.longitude, this.radius, "km");
            setCacheKey(tripoto);
            TripsRequest mTripsRequest = new TripsRequest(tripoto);
            this.requestInProgress = true;
            mSpiceManager.execute(mTripsRequest, this.cacheKey, Value.ONE_DAY, this);
        } else {
            final Snackbar noInternetSnackbar = Snackbar.make(rootView, "No internet connection", Snackbar.LENGTH_LONG);
            noInternetSnackbar.setAction("Open Settings", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Router.showSettingsActivity(HomeActivity.this);
                    noInternetSnackbar.dismiss();
                }
            });
            noInternetSnackbar.show();
        }
    }

    private void setCacheKey(Tripoto tripoto) {
        this.cacheKey = "offset=" + tripoto.offset + "&limit=" + tripoto.limit
                + "&lat=" + tripoto.latitude + "&lng=" + tripoto.longitude + "&radius=" + this.radius + "&unit=km";
    }

    private BroadcastReceiver brParameters = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            HomeActivity.this.radius = intent.getIntExtra(Key.RADIUS, HomeActivity.this.radius);
            double latidue = intent.getDoubleExtra(Key.LATITUDE, Value.LAT_NOT_SET);
            double longitude = intent.getDoubleExtra(Key.LONGITUDE, Value.LNG_NOT_SET);
            dataset.clear();
            mTripsAdapter.notifyDataSetChanged();
            pbView.setVisibility(View.VISIBLE);
            if (latidue == Value.LAT_NOT_SET || longitude == Value.LNG_NOT_SET) {
                Log.d(TAG, "here");
                doActionOnPermission();
            } else {
                HomeActivity.this.latitude = latidue;
                HomeActivity.this.longitude = longitude;
                makeRequest();
            }
        }
    };

    @Override
    public void onLocationChanged(Location location) {
        if(location != null) {
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
            Log.d(TAG, "lat= " + latitude + " lng= " + longitude);
            makeRequest();
        }
    }
}
