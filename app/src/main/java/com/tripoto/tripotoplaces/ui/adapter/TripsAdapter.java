package com.tripoto.tripotoplaces.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tripoto.tripotoplaces.R;
import com.tripoto.tripotoplaces.model.response.TripsData;
import com.tripoto.tripotoplaces.ui.viewholder.TripsViewHolder;

import java.util.ArrayList;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class TripsAdapter extends RecyclerView.Adapter<TripsViewHolder> {

    private Context mContext;
    private ArrayList<TripsData> dataset;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;

    public TripsAdapter(Context context, ArrayList<TripsData> dataset) {
        this.mContext = context;
        this.dataset = dataset;
        this.imageLoader = ImageLoader.getInstance();
        this.displayImageOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .showImageOnLoading(R.drawable.ic_launcher).build();
    }

    @Override
    public TripsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_trip_card, parent, false);
        return new TripsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TripsViewHolder holder, int position) {
        TripsData data = dataset.get(position);
        this.imageLoader.displayImage(data.userImageUrl, holder.ivUserImage, this.displayImageOptions);
        this.imageLoader.displayImage(data.tripImageUrl, holder.ivTripImage, this.displayImageOptions);
        holder.tvTripName.setText(data.tripName);
        holder.tvUserName.setText(data.userName);
        holder.tvUserHandle.setText(data.userHandle);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
