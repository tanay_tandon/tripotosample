package com.tripoto.tripotoplaces.ui.fragment;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.tripoto.tripotoplaces.R;
import com.tripoto.tripotoplaces.config.constants.Key;
import com.tripoto.tripotoplaces.config.constants.Value;
import com.tripoto.tripotoplaces.util.PrefUtils;

import java.io.IOException;
import java.util.List;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class ParametersFragment extends DialogFragment implements View.OnClickListener {


    public static final String TAG = ParametersFragment.class.getName();


    private EditText etLocation;
    private SeekBar sbRadius;
    private TextView tvRadiusValue;
    private RadioButton rbUseCurrentLocation;
    private int noOfClicks = 0;


    public static ParametersFragment newInstance() {
        ParametersFragment parametersFragment = new ParametersFragment();
        return parametersFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_parameters, container, false);
        getDialog().setTitle("Set Location and Radius");
        this.rbUseCurrentLocation = (RadioButton) view.findViewById(R.id.rb_use_current_location);
        this.rbUseCurrentLocation.setOnClickListener(this);
        this.etLocation = (EditText) view.findViewById(R.id.et_location_name);
        String lastLocation = PrefUtils.getLastLocation();
        if(lastLocation != null) {
            this.etLocation.setText(lastLocation);
        }
        this.sbRadius = (SeekBar) view.findViewById(R.id.sb_set_radius);
        int radius = PrefUtils.getRadius();
        this.sbRadius.setProgress(radius / 5);
        this.tvRadiusValue = (TextView) view.findViewById(R.id.tv_radius);
        this.tvRadiusValue.setText((this.sbRadius.getProgress() * 5) + "km");

        this.rbUseCurrentLocation.setChecked(PrefUtils.isUsingCurrentLocation());
        this.etLocation.setVisibility(this.rbUseCurrentLocation.isChecked() ? View.GONE : View.VISIBLE);
        this.noOfClicks = PrefUtils.isUsingCurrentLocation() ? 0 : 1;

        view.findViewById(R.id.btn_cancel).setOnClickListener(this);
        view.findViewById(R.id.btn_confirm).setOnClickListener(this);

        this.sbRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvRadiusValue.setText((sbRadius.getProgress() * 5) + "km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_cancel: {
                getDialog().dismiss();
                break;
            }

            case R.id.btn_confirm:{
                if(this.rbUseCurrentLocation.isChecked()) {
                    sendLatLngAndRadius(Value.LAT_NOT_SET, Value.LNG_NOT_SET, sbRadius.getProgress() * 5);
                } else {
                    String locationName = etLocation.getText().toString();
                    if (locationName != null && locationName.length() != 0) {
                        PrefUtils.setLastLocation(locationName);
                        new GetLatLngFromLocationNameTask().execute(locationName);
                    } else {
                        Snackbar.make(etLocation, "Refine your query please", Snackbar.LENGTH_LONG).show();
                    }
                }
                break;
            }

            case R.id.rb_use_current_location: {
                noOfClicks++;
                this.rbUseCurrentLocation.setChecked(noOfClicks % 2 == 0);
                PrefUtils.setUsingCurrentLocation(this.rbUseCurrentLocation.isChecked());
                this.etLocation.setVisibility(this.rbUseCurrentLocation.isChecked() ? View.GONE : View.VISIBLE);
                break;
            }
        }
    }

    private void sendLatLngAndRadius(double latitude, double longitude, int radius) {
        Intent intent = new Intent(getActivity().getResources().getString(R.string.intent_filter_set_parameters));
        intent.putExtra(Key.LATITUDE, latitude);
        intent.putExtra(Key.LONGITUDE, longitude);
        intent.putExtra(Key.RADIUS, radius);
        PrefUtils.setRadius(radius);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        getDialog().dismiss();
    }

    private class GetLatLngFromLocationNameTask extends AsyncTask<String, Void, LatLng> {
        @Override
        protected LatLng doInBackground(String... params) {
            String locationName = params[0];
            Geocoder geocoder = new Geocoder(getActivity());
            try {
                List<Address> addresses =  geocoder.getFromLocationName(locationName, 1);
                if(addresses.size() == 0) {
                    return null;
                }
                Address address = addresses.get(0);
                return new LatLng(address.getLatitude(), address.getLongitude());
            } catch (IOException ioexception) {
                Log.d(TAG, "The address was wrong");
                return null;
            }
        }
        @Override
        protected void onPostExecute(LatLng result) {
            if (result != null) {
                sendLatLngAndRadius(result.latitude, result.longitude, sbRadius.getProgress() * 5);
            } else {
                Snackbar.make(etLocation, "Refine your query please", Snackbar.LENGTH_LONG).show();
            }
        }
    }

}
