package com.tripoto.tripotoplaces.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tripoto.tripotoplaces.R;

/**
 * Created by tanaytandon on 24/01/16.
 */
public class TripsViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivUserImage;
    public TextView tvUserName;
    public TextView tvUserHandle;
    public ImageView ivTripImage;
    public TextView tvTripName;

    public TripsViewHolder(View itemView) {
        super(itemView);
        this.ivUserImage = (ImageView) itemView.findViewById(R.id.iv_user_image);
        this.tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);
        this.tvUserHandle = (TextView) itemView.findViewById(R.id.tv_user_handle);
        this.ivTripImage = (ImageView) itemView.findViewById(R.id.iv_trip_image);
        this.tvTripName = (TextView) itemView.findViewById(R.id.tv_trip_name);
    }
}
